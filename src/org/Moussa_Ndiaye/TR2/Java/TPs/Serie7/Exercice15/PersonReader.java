package org.Moussa_Ndiaye.TR2.Java.TPs.Serie7.Exercice15;
import
        java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PersonReader {

    public static List<Person> reader(String fileName){

        List<Person> listPersonne=new ArrayList<>();
        try (FileReader fr = new FileReader(fileName); BufferedReader br = new BufferedReader(fr);)
        {
            listPersonne=br.lines()
                    .filter(l->!l.startsWith("#"))
                    .map(l->getPerson(l))
                    .collect(Collectors.toList());

        }
        catch (IOException e){
            System.out.println("e.getMessage() = " + e.getMessage());
        }
        return listPersonne;

    }

    public static List<Person> readerFlatMap(String fileName){

        List<Person> listPersonne=new ArrayList<>();
        try (FileReader fr = new FileReader(fileName); BufferedReader br = new BufferedReader(fr);)
        {
            listPersonne=br.lines()
                    .flatMap(l->(!l.startsWith("#"))?Stream.of(getPerson(l)):Stream.empty())
                    .collect(Collectors.toList());

        }
        catch (IOException e){
            System.out.println("e.getMessage() = " + e.getMessage());
        }
        return listPersonne;

    }

    static Person getPerson(String l) {
        String liste[] = l.split(",");
        String lastName = liste[0];
        String firstName = liste[1];
        int age = Integer.parseInt(liste[2].trim());
        return new Person(lastName, firstName, age);
    }
}
