package org.Moussa_Ndiaye.TR2.Java.TPs.Serie7.Exercice15;


import java.util.List;
import java.util.function.Function;


public class Exercice15 {


    public static void main(String[] args) {

        Function<String,Person> recupPerson= PersonReader::getPerson;

        System.out.println(recupPerson.apply("Auguin, Christophe, 38").toString());

        //Reccuperation par filtrage
        List<Person> listPersonne=PersonReader.reader("Person.txt");
        System.out.println("Utilisation des filtres");
        listPersonne.forEach(System.out::println);


        //Utilisation de flatMap
        List<Person> listPersonne2=PersonReader.readerFlatMap("Person.txt");
        System.out.println("Utilisation de FlatMap");
        listPersonne2.forEach(System.out::println);

        //Ecriture des personnes sur un fichier
        PersonWriter.write(listPersonne,"Ecrire.txt");
    }
}
