package org.Moussa_Ndiaye.TR2.Java.TPs.Serie7.Exercice15;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

public class PersonWriter {

    public static void write(List<Person> people, String fileName){
        List<String> listPersonne;
        try (FileWriter fw = new FileWriter(fileName); BufferedWriter bw = new BufferedWriter(fw);)
        {
            listPersonne=people.stream().map(Person::toString).collect(Collectors.toList());
            for (String p: listPersonne) {
                bw.write(p);
                bw.write("\n");
            }
            bw.flush();

        }
        catch (IOException e){
            System.out.println("e.getMessage() = " + e.getMessage());
        }


    }
}
