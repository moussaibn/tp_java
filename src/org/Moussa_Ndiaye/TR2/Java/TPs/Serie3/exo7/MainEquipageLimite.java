package org.Moussa_Ndiaye.TR2.Java.TPs.Serie3.exo7;

import java.util.ArrayList;
import java.util.Collection;

public class MainEquipageLimite {
	public static void main(String args[]) {
		
		// partie equipage avec limite
		
		EquipageWithLimite eq_With_Lim= new EquipageWithLimite(4);
		eq_With_Lim.addMarin(new Marin("Moussa","Ndiaye",2800));
		eq_With_Lim.addMarin(new Marin("Alioune","Camara",3000));
		eq_With_Lim.addMarin(new Marin("Dinesh","G",2400));
		eq_With_Lim.addMarin(new Marin("Nicolas","Rannou",1500));
		System.out.println("TEST ADD: \n"+eq_With_Lim.toString());
		
		Collection <Marin> newEquipe_w_limit=new ArrayList<>();
		newEquipe_w_limit.add(new Marin("lenovo","thinkpad",800));
		newEquipe_w_limit.add(new Marin("Alioune","Camara",3000));
		
		eq_With_Lim.addAllEquippage(newEquipe_w_limit);
		
		System.out.println("Test du addALL:\n"+eq_With_Lim.toString());
		eq_With_Lim.clear();
		System.out.println("TEst clear: \n"+eq_With_Lim.toString());

	}
}
