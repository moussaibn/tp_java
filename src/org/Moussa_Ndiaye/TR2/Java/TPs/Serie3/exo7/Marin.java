package org.Moussa_Ndiaye.TR2.Java.TPs.Serie3.exo7;

public class Marin {
	private String nom;
	private String prenom;
	private int salaire;
	
	Marin(String nom,String prenom,int salaire){
		this.nom=nom;
		this.prenom=prenom;
		this.salaire=salaire;
	}
	
	Marin(String nom,int salaire){
		this.nom=nom;
		this.prenom="";
		this.salaire=salaire;
	}
	
	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPreom(String prenom) {
		this.prenom = prenom;
	}
	
	public int getSalaire
() {
		return this.salaire;
	}

	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}
	
	public void augmenteSalaire(int augmentation) {
		setSalaire(this.salaire+augmentation);
	}
	
	public String toString() {
		String chaine;
		chaine="Prenom :"+this.prenom+"\n"+
				"Nom :"+this.nom+"\n"+
				"Salaire :"+this.salaire+"\n";
		return chaine;
		
	}
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
	  if (!(obj  instanceof Marin))  
	         return false ;  
	      
	    Marin marin = (Marin)obj ;  
	      
	     return (this.nom.equals(marin.nom) &&
	    		 this.prenom.equals(marin.prenom) &&
	    		 this.salaire == marin.salaire) ;  
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		int hash=15;
		hash =  20 * hash + ((nom == null) ?  0 : nom.hashCode()) ;  
	    hash =  20 * hash + ((prenom == null) ?  0 : prenom.hashCode()) ;  
	    hash =  20 * hash + salaire ;
	     return hash ;
	}
	
}

