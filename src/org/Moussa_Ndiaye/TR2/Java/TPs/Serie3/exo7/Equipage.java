package org.Moussa_Ndiaye.TR2.Java.TPs.Serie3.exo7;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Equipage {
	private Collection<Marin> Marins; 
	
	public Equipage() {
		Marins=new ArrayList<>();
	}
	
	
	public boolean addMarin(Marin m) {
		 return this.Marins.add(m);
	}
	
	public boolean removeMarin(Marin m) {
		 return this.Marins.remove(m);
	}
	
	public boolean isMarinPresent(Marin m) {
		 return this.Marins.contains(m);
	}
	
	public String toString() {
		Iterator <Marin> it=Marins.iterator();
		String chaine;
		chaine="Le nombre de marins est "+Marins.size()+"\n";
		int i=0;
		while(it.hasNext()) {
			chaine+="\nMarin "+i+"\n";
			chaine+=it.next().toString();
			i++;
		}
		return chaine;
	}
	
	public void addAllEquippage(Collection<Marin> m) {	
		Iterator <Marin> it=m.iterator();
		while(it.hasNext()) {
			Marin tmp=it.next();
			if(!(this.isMarinPresent(tmp))) 
				this.addMarin(tmp);			
		}	
	}
	
	public void clear() {
		this.Marins.clear(); 
	}
	
	public int getNombreMarins() {
		return this.Marins.size();
	}
	
	public double getSalaireMoyen() {
		Iterator <Marin> it=this.Marins.iterator();
		double moy=0.0;
		while(it.hasNext()) {
			moy+=it.next().getSalaire();	
		}
		return moy/this.getNombreMarins();
	}
	
}
