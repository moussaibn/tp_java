package org.Moussa_Ndiaye.TR2.Java.TPs.Serie3.exo7;

import java.util.ArrayList;
import java.util.Collection;

public class Main {
	public static void main(String args[]) {
		Equipage equipe= new Equipage();
		equipe.addMarin(new Marin("Moussa","Ndiaye",2800));
		equipe.addMarin(new Marin("Alioune","Camara",3000));
		equipe.addMarin(new Marin("Dinesh","G",2400));
		equipe.addMarin(new Marin("Nicolas","Rannou",1500));
		System.out.println("TEST ADD:\n"+equipe.toString());
		
		Collection <Marin> newEquipe=new ArrayList<>();
		newEquipe.add(new Marin("lenovo","thinkpad",800));
		newEquipe.add(new Marin("Alioune","Camara",3000));
		
		equipe.addAllEquippage(newEquipe);
		
		System.out.println("Test du addALL:\n"+equipe.toString());
		equipe.clear();
		System.out.println("TEst clear:\n"+equipe.toString());
		

	}
}
