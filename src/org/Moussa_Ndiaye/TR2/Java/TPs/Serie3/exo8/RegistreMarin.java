package org.Moussa_Ndiaye.TR2.Java.TPs.Serie3.exo8;

import java.util.ArrayList;
import java.util.HashMap;

public class RegistreMarin {
	private HashMap<Integer, ArrayList<Marin> > map;
	
	public RegistreMarin() {
		map=new HashMap<Integer, ArrayList<Marin>>();

	}
	
	public void addMarin(Marin m) {
		int cle=(m.getNaissance()/10)*10;
		ArrayList<Marin> marins=new ArrayList<>(); 
		if(this.map.isEmpty()|| !this.map.isEmpty() && !this.map.containsKey(cle)) {
			marins.add(m);
			this.map.put(cle,marins);
		}
		else
			this.map.get(cle).add(m);
	}
	
	public ArrayList<Marin> get(int annee) {
		ArrayList<Marin> mapResultat= new ArrayList<>(); 
		
		mapResultat= this.map.get(annee);
		
		return mapResultat;
	}
	
	
	public int count(int annee) {
		return this.map.get(annee).size();
	}
	
	
	public int count() {
		int nbMarin=0;
		for(HashMap.Entry<Integer, ArrayList<Marin>> grp:this.map.entrySet())  
			nbMarin+=count(grp.getKey());		
		
		return nbMarin;
	}
	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub

		return "RegistreMarin [map=" + map + "]";
	}

}
