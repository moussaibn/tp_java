package org.Moussa_Ndiaye.TR2.Java.TPs.Serie3.exo8;



public class Main {
	public static void main(String args[]) {
		RegistreMarin registre=new RegistreMarin();

		System.out.println("Affichage du contenu du registre:" + registre.count()+" marins");
		
		System.out.println("Test toString:\n"+registre.toString());
		registre.addMarin(new Marin("Lamazou","Titouan",1955));
		registre.addMarin(new Marin("Gautier","Alain",1962));
		registre.addMarin(new Marin("Auguin","Christophe",1959));
		registre.addMarin(new Marin("Le Cle","Armel",1977));
		registre.addMarin(new Marin("Michel", "Desjoyeaux", 1965));
		registre.addMarin(new Marin("Vincent", "Riou", 1972));
		registre.addMarin(new Marin("Fran�ois", "Gabart", 1983));

		System.out.println("TEST addMarin\n" + registre.count()+" marins\n"+registre.toString());
		
		System.out.println("Les Marins "+registre.count(1960)+" n�s dans les annees 60");
		System.out.println(registre.get(1960).toString());

		
	}
}
