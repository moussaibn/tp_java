package org.Moussa_Ndiaye.TR2.Java.TPs.Serie3.exo8;

public class Marin {
	private String nom;
	private String prenom;
	private int naissance;
	
	Marin(String nom,String prenom,int naissance){
		this.nom=nom;
		this.prenom=prenom;
		this.naissance=naissance;
	}
	
	Marin(String nom,int naissance){
		this.nom=nom;
		this.prenom="";
		this.naissance=naissance;
	}
	
	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPreom(String prenom) {
		this.prenom = prenom;
	}
	
	public int getNaissance() 
	{
		return this.naissance;
	}

	public void setNaissance(int naissance) {
		this.naissance = naissance;
	}
	
	
	public String toString() {
		String chaine;
		chaine="Prenom :"+this.prenom+"\n"+
				"Nom :"+this.nom+"\n"+
				"Annee naissance :"+this.naissance+"\n";
		return chaine;
		
	}
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
	  if (!(obj  instanceof Marin))  
	         return false ;  
	      
	    Marin marin = (Marin)obj ;  
	      
	     return (this.nom.equals(marin.nom) &&
	    		 this.prenom.equals(marin.prenom) &&
	    		 this.naissance == marin.naissance) ;  
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		int hash=15;
		hash =  20 * hash + ((nom == null) ?  0 : nom.hashCode()) ;  
	    hash =  20 * hash + ((prenom == null) ?  0 : prenom.hashCode()) ;  
	    hash =  20 * hash + naissance ;
	     return hash ;
	}
	
}

