package org.Moussa_Ndiaye.TR2.Java.TPs.Serie1.exo2;

import java.math.BigInteger;


public class Factorielle {

	int intFactorielle(int a){
		if(a==0)
			return 1; 
		return a* intFactorielle(a-1);
	}
	
	double doubleFactorielle(double a){
		if(a==0)
			return 1; 
		return a* doubleFactorielle(a-1);
	}
	
	BigInteger bigIntFactorielle(BigInteger a){
		if(a.equals(new BigInteger("0")))
			return new BigInteger("1"); 
		return a.multiply(bigIntFactorielle(a.subtract(new BigInteger("1"))));
	}

}


