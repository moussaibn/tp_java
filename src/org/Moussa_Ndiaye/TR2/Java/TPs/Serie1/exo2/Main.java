package org.Moussa_Ndiaye.TR2.Java.TPs.Serie1.exo2;

import java.math.BigInteger;

public class Main {
	public static void main(String[] args) {
		Factorielle fact = new Factorielle() ;
		int i=10;
		double d=13;
		BigInteger b=new BigInteger("25");
		System.out.println("Factorielle d'un entier: 10! = " +		fact.intFactorielle(i)) ;
		System.out.println("Factorielle d'un double: 13! = " +		fact.doubleFactorielle(d)) ;
		System.out.println("Factorielle d'un BigInteger 25! = " +		fact.bigIntFactorielle(b)) ;
		}
}
