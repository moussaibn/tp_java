package org.Moussa_Ndiaye.TR2.Java.TPs.Serie1.exo4;

public class Palindrome {
	
	boolean palindrome(String s) {
		s=s.replaceAll(" ","");
		s=s.toLowerCase();
		if(s.length()==1 || s.length()==0) {
			return true;
		}
		else 
			if(s.charAt(0)==s.charAt(s.length()-1))
				return palindrome(s.substring(1,s.length()-1));
			else {

				return false;	
			}
	}	
}
