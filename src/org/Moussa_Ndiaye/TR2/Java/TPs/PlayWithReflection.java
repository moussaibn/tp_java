package org.Moussa_Ndiaye.TR2.Java.TPs;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

public class PlayWithReflection {

    public static void main(String[] args) {

        try {
            Class classPerson = Class.forName("org.Moussa_Ndiaye.TR2.Java.TPs.Serie7.Exercice15.Person");
            List<Field> fields = List.of(classPerson.getDeclaredFields());
            fields.forEach(f -> System.out.println(f.getName()));

            //Les methodes
            //devops france e tsponsoor

              List<Method> methods = List.of(classPerson.getDeclaredMethods());
            Method getAge=classPerson.getMethod("getAge",int.class);
            methods.forEach(m -> System.out.println(m.getName() + " " + m.getParameterCount() + " params"));

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
//

    }
}
