package org.Moussa_Ndiaye.TR2.Java.TPs.Serie2.exo6;

public class MarinUtil {
	
	public void augmenteSalaire(Marin[] marins, int pourcentage) {
		for(int i=0;i<marins.length;i++)
			marins[i].setSalaire(marins[i].getSalaire()*(100+pourcentage)/100);
	}
	
	public int getMaxSalaire(Marin[] marins) {
		int pos_max=0;
		for(int i=1;i<marins.length;i++)
			if(marins[pos_max].getSalaire()<marins[i].getSalaire())
				pos_max=i;
		return marins[pos_max].getSalaire();
	}
	
	public double getMoyenneSalaire(Marin[] marins) {
		double moy=0;
		for(int i=1;i<marins.length;i++)
			moy+=marins[i].getSalaire();
		return moy/marins.length;
	}
	
	public double getMedianeSalaire(Marin[] marins) {
		double med;
		int ech,pos_med;
		int Tableau[]= new int[marins.length];
		for(int i=0;i<marins.length;i++)
			Tableau[i]=marins[i].getSalaire();
		for(int i=0;i<marins.length;i++)
			for(int j=0;j<i-1;j++)
				if(Tableau[j]>Tableau[j+1])
				{
					ech=Tableau[j];
					Tableau[j]=Tableau[j+1];
					Tableau[j+1]=ech;
				}
		pos_med=marins.length;
		if(pos_med%2==1)
			med= marins[pos_med/2].getSalaire();
		else
			med=(marins[pos_med/2].getSalaire()+marins[pos_med/2 + 1].getSalaire())/2.0;
		return med;
	}

}
