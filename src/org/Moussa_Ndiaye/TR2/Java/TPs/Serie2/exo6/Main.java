package org.Moussa_Ndiaye.TR2.Java.TPs.Serie2.exo6;

public class Main {
	public static void main(String args[]) {
		Marin m[]=new Marin[3];
		m[0]=new Marin("Moussa","Ndiaye",2800);
		m[1]=new Marin("Alioune","Camara",3000);
		m[2]=new Marin("Nicolas","Rannou",1500);

		for(int i=0;i<m.length;i++)
			System.out.println(m[i].toString());
		
		MarinUtil mu= new MarinUtil();
		mu.augmenteSalaire(m,15);
		
		System.out.println("Apres une augmentation de 15%");
		for(int i=0;i<m.length;i++)
			System.out.println(m[i].toString());

		System.out.println("Le salaire maximum est "+mu.getMaxSalaire(m));

		System.out.println("Le salaire moyen est "+mu.getMoyenneSalaire(m));

		System.out.println("Le salaire median est "+mu.getMedianeSalaire(m));


	}
}
