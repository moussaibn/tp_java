package org.Moussa_Ndiaye.TR2.Java.TPs.Serie2.exo5;

public class Main {
	public static void main(String args[]) {
		Marin m1=new Marin("Moussa","Ndiaye",2500);
		Marin m2=new Marin("Moussa","Ndiaye",2500);
		Marin m3=new Marin("Martin","Matin",500);
		System.out.println("Marin m1:\n"+m1.toString());
		System.out.println("Marin m2:\n"+m2.toString());
		System.out.println("Marin m3:\n"+m3.toString());
		System.out.println("Les marins m1 et m2 sont egaux "+m1.equals(m2));
		System.out.println("Les marins m1 et m3 sont egaux "+m1.equals(m3));

		System.out.println("\nAffichage des hashcode");
		System.out.println("Pour m1:"+m1.hashCode());
		System.out.println("Pour m2:"+m2.hashCode());
		System.out.println("Pour m3:"+m3.hashCode());
	}
}
