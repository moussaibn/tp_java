package org.Moussa_Ndiaye.TR2.Java.TPs.Serie9.Exercice18;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AnalyzeBean {
    static String getClassName(Object o) {
        return o.getClass().getName();
    }

    private static Class getInstance(String className) {
        Class clazz = null;
        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return clazz;
    }

    static List<String> getProperties(Object o) {

        Class clazz = getInstance(getClassName(o));
        return Stream.of(clazz.getMethods()).
                map(Method::getName)
                .filter(prop -> (prop.startsWith("get") || prop.startsWith("is")))
                .flatMap(prop -> {
                    if (prop.startsWith("is"))
                        return Stream.of(prop.substring(2, 3).toLowerCase() + prop.substring(3));
                    return Stream.of(prop.substring(3, 4).toLowerCase() + prop.substring(4));
                })
                .collect(Collectors.toList());
    }

    public static Object get(Object bean, String property) {
        Class clazz = getInstance(getClassName(bean));
        Object value = null;
        String getter = "get" + property.substring(0, 1).toUpperCase() + property.substring(1);
        try {
            value = clazz.getMethod(getter).invoke(bean);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return value;
    }

    public static void set(Object bean, String property, int value) {
        Class clazz = getInstance(getClassName(bean));
        Method setter;
        String set = "set" + property.substring(0, 1).toUpperCase() + property.substring(1);

        try {
            setter = clazz.getMethod(set, int.class);
            setter.invoke(bean, value);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static void set(Object bean, String property, Object value) {
        Class clazz = getInstance(getClassName(bean));
        Method setter;
        String set = "set" + property.substring(0, 1).toUpperCase() + property.substring(1);

        try {
            setter = clazz.getMethod(set, value.getClass());
            setter.invoke(bean, value);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static List<Object> read(String fileName) {
        String ligne;
        List<Object> liste = new ArrayList<>();
        Object obj = null;
        boolean isLastObject = false;
        String property;
        Object value;
        try (FileReader fr = new FileReader(fileName); BufferedReader br = new BufferedReader(fr)) {
            while ((ligne = br.readLine()) != null) {
                if (!ligne.startsWith("#")) {
                    if (ligne.startsWith("bean")) {
                        if (obj != null) {
                            liste.add(obj);
                            isLastObject = false;
                        }
                    } else {
                        property = extractProperty(ligne);
                        value = extractValue(ligne);
                        if (property.startsWith("cla"))
                            obj = getInstance((String) value).newInstance();
                        else {
                            if (property.equals("salary") || property.equals("age"))
                                set(obj, property, Integer.parseInt(value.toString()));
                            else
                                set(obj, property, value);
                            isLastObject = true;
                        }
                    }
                }
            }
            if (isLastObject)
                liste.add(obj);
        } catch (IOException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return liste;
    }


    private static String extractProperty(String line) {
        return line.split("=")[0].split("\\.")[1].trim();
    }

    private static Object extractValue(String line) {
        return line.split("=")[1].trim();
    }

     static void write(List<Object> beans, String fileName) {
        int count = 0;
        String chaine;
        try (FileWriter fw = new FileWriter(fileName);
             BufferedWriter bw = new BufferedWriter(fw)) {
            bw.write("#Commentaire\n");
            for (Object o : beans) {
                count++;
                List<Method> methods = List.of(getInstance(getClassName(o)).getMethods());
                int finalCount = count;

                List <String> champs = methods.stream().filter(m -> m.getName().startsWith("get"))
                        .map(m -> ("p" + finalCount + "." + fieldNameFrom(m.getName()) + "=" + get(o, fieldNameFrom(m.getName())) + "\n"))
                        .collect(Collectors.toList());
                Collections.reverse(champs);
                chaine= String.join("", champs);
                bw.write("beans.name=p" + count + " \n" + chaine);
            }
            bw.flush();
        } catch (IOException e) {
            System.out.println("e.getMessage() = " + e.getMessage());
        }
        System.out.println("   Ecriture de " + beans.size() + " objets termines ;-)");
    }

    private static String fieldNameFrom(String methodName) {
        return methodName.substring(3, 4).toLowerCase() + methodName.substring(4);
    }
}

