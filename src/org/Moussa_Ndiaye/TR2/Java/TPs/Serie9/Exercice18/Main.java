package org.Moussa_Ndiaye.TR2.Java.TPs.Serie9.Exercice18;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        Person p = new Person("Ndiaye", "Moussa", 24);
        Employee e = new Employee("Mbapee", "Kilyan", 20, 38000);
        System.out.println("1 - Classe de p = " + AnalyzeBean.getClassName(p));
        System.out.println("2 - Classe de e = " + AnalyzeBean.getClassName(e));
        System.out.println("3 - La liste des proprietes  :");
        AnalyzeBean.getProperties(e).forEach(prop -> System.out.println(" -" + prop));
        System.out.println("4 - Recuperatoin du salaire de " + e.getFirstName() + " via la commande get : " + AnalyzeBean.get(e, "salary"));
        AnalyzeBean.set(e, "salary", 100000);
        System.out.println("5 - Modification du salaire de " + e.getFirstName() + " via la commande set : " + AnalyzeBean.get(e, "salary"));
        System.out.println("6 - Lecture d'objet depuis le fichier 'Serie9Read.txt' ");
        AnalyzeBean.read("Serie9Read.txt").forEach(o -> System.out.println("  Object: " + o.toString()));
        List<Object> liste = List.of(p, e);
        System.out.println("7 - Ecriture d'objet sur le fichier 'Serie9Write.txt' ");
        AnalyzeBean.write(liste, "Serie9Write.txt");

    }
}
