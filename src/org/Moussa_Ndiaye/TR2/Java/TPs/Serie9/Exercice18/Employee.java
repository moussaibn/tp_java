package org.Moussa_Ndiaye.TR2.Java.TPs.Serie9.Exercice18;

public class Employee extends Person {
    private int salary;

    public Employee(String lastName, String firstName, int age, int salary) {
        super(lastName, firstName, age);
        this.salary = salary;
    }

    public Employee() {
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String toString() {
        return super.toString() + ", " + salary;
    }
}
