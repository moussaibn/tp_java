package org.Moussa_Ndiaye.TR2.Java.TPs.Serie8.Exercice16;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class PersonBinReader {
    public static List<Person> readBinaryFields(String fileName){

        List<Person> listPersonne=new ArrayList<>();
        try (FileInputStream fis = new FileInputStream(fileName);
             BufferedInputStream bis = new BufferedInputStream(fis);
             DataInputStream dis = new DataInputStream(bis);)
        {
            int taille= dis.readInt();
            for (int i=0;i<taille;i++){
                Person p=new Person();
                p.setLastName(dis.readUTF());
                p.setFirstName(dis.readUTF());
                p.setAge(dis.readInt());
                listPersonne.add(p);
            }
        }
        catch (IOException e){
            System.out.println("e.getMessage() = " + e.getMessage());
        }
        return listPersonne;
    }
}
