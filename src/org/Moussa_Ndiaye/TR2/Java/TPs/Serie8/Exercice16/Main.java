package org.Moussa_Ndiaye.TR2.Java.TPs.Serie8.Exercice16;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Main {
    public static void main(String[] args) {

        Person p = new Person("Ndiaye", "Moussa", 24);
        Person p1 = new Person("Mbapee", "Kilyan", 20);
        Person p2 = new Person("Camara", "Alioune Badara", 21);

        List<Person> listePers = List.of(p, p1,p2);
        System.out.println("2 - Ecriture d'objet sur le fichier 'Serie8.txt' ");
        PersonBinWriter.writeBinaryFields(listePers, "Serie8.txt");

        System.out.println("3 - Lecture d'objet depuis le fichier 'Serie8.txt' ");
        List<Person> personLu = PersonBinReader.readBinaryFields("Serie8.txt");
        personLu.forEach(l -> System.out.println("  Person = " + l.toString()));
    }

    static Function<Person, byte[]> personToByte = p -> {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);
        try {
            dos.writeUTF(p.getLastName());
            dos.writeUTF(p.getFirstName());
            dos.writeInt(p.getAge());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bos.toByteArray();
    };
}
