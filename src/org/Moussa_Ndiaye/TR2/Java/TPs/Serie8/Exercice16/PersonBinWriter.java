package org.Moussa_Ndiaye.TR2.Java.TPs.Serie8.Exercice16;

import java.io.*;
import java.util.List;

public class PersonBinWriter {

    public static void writeBinaryFields(List<Person> people, String fileName) {
        try (FileOutputStream fos = new FileOutputStream(fileName);
             BufferedOutputStream bos = new BufferedOutputStream(fos);
             DataOutputStream dos = new DataOutputStream(bos);)
        {
            dos.writeInt(people.size());
            for (Person p : people)
                dos.write(Main.personToByte.apply(p));
            dos.flush();
            System.out.println("Ecriture de " + people.size() + " Person termines");

        }
        catch(IOException e) {
            System.out.println("e.getMessage() = " + e.getMessage());
        }
    }
}
