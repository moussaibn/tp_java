package org.Moussa_Ndiaye.TR2.Java.TPs.Serie8.Exercice17;

import java.io.*;
import java.util.List;

public class PersonBinWriter {

    public static void writeBinaryObject(List<Person> people, String fileName) {
        try (FileOutputStream fos = new FileOutputStream(fileName);
             BufferedOutputStream bos = new BufferedOutputStream(fos);
             ObjectOutputStream oos = new ObjectOutputStream(bos);) {
            oos.writeInt(people.size());
            for (Person p : people)
                oos.writeObject(p);
            oos.flush();
            System.out.println("Ecriture de " + people.size() + " Person termines");
        } catch (IOException e) {
            System.out.println("e.getMessage() = " + e.getMessage());
        }
    }
}
