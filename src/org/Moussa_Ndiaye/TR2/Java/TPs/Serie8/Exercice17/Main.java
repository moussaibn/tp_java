package org.Moussa_Ndiaye.TR2.Java.TPs.Serie8.Exercice17;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        Person p = new Person("Ndiaye", "Moussa", 24);
        Person p1 = new Person("Mbapee", "Kilyan", 20);
        Person p2 = new Person("Camara", "Alioune Badara", 21);

        List<Person> listePers = List.of(p, p1,p2);
        System.out.println("3 - Ecriture d'objet sur le fichier 'Serie8.txt' ");
        PersonBinWriter.writeBinaryObject(listePers, "Serie8.txt");

        System.out.println("4 - Lecture d'objet depuis le fichier 'Serie8.txt' ");
        List<Person> personLu = PersonBinReader.readBinaryObject("Serie8.txt");
        personLu.forEach(l -> System.out.println("  Person = " + l.toString()));

    }

}
