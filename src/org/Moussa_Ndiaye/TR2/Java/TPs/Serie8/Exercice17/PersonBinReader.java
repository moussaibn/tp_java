package org.Moussa_Ndiaye.TR2.Java.TPs.Serie8.Exercice17;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class PersonBinReader {
    public static List<Person> readBinaryObject(String fileName){

        List<Person> listPersonne=new ArrayList<>();
        try (FileInputStream fis = new FileInputStream(fileName);
             BufferedInputStream bis = new BufferedInputStream(fis);
             ObjectInputStream ois = new ObjectInputStream(bis);)
        {
            int taille= ois.readInt();
            for (int i=0;i<taille;i++){
                Person p = (Person) ois.readObject();
                listPersonne.add(p);
            }
        }
        catch (IOException | ClassNotFoundException e){
            System.out.println("e.getMessage() = " + e.getMessage());
        }
        return listPersonne;
    }
}
