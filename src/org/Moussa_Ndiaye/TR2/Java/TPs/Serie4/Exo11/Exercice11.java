package org.Moussa_Ndiaye.TR2.Java.TPs.Serie4.Exo11;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public class Exercice11 {

    public static void main(String[] args) {
        Comparator<String> cmpLength = (ch1, ch2) -> (Integer.compare(ch1.length(), ch2.length()));

        Function<Person, String> getLastName = Person::getLastName;

        Function<Person, String> getFirstName = Person::getFirstName;

        Comparator<Person> cmpLastName = comparing(getLastName);

        Comparator<Person> cmpFirstName = comparing(getFirstName);

        Comparator<Person> cmpPerson = cmpLastName.thenComparing(cmpFirstName);

        Comparator<Person> cmpPersonReverse = cmpPerson.reversed();


        Person p0 = new Person("Zinedine", "Zidane", 44);
        Person p1 = new Person("Moussa", "Ndiaye", 24);
        Person p2 = new Person("Meissa", "Ndiaye", 25);
        Person p3 = new Person("Aliou", "Camara", 21);

        List<Person> listePersonne=new ArrayList<>();
        listePersonne.add(null);
        listePersonne.add(p1);
        listePersonne.add(p2);
        listePersonne.add(null);
        listePersonne.add(p3);
        listePersonne.add(p0);
        listePersonne.add(null);
        listePersonne.add(null);
        System.out.println("1- Comparateur de longueur de chaines de caractere");
        System.out.println(p1.getFirstName() +((cmpLength.compare(p1.getFirstName(),p2.getFirstName())>0)?" > ":(cmpLength.compare(p1.getFirstName(),p2.getFirstName())<0?" < ":" = "))+ p3.getFirstName() );

        System.out.println("\n2- Comparateur de parsonne en, fonction du lastname");
        System.out.println("p1: " + p1.toString() +((cmpLastName.compare(p1,p2)>0)?" > ":(cmpLastName.compare(p1,p2)<0?" < ":" = "))+ "et p2 " + p2.toString());

        System.out.println("\n3- Comparateur de parsonne en, fonction du lastname puis en fonction du firstName");
        System.out.println("p1: " + p1.toString() +((cmpPerson.compare(p1,p2)>0)?" > ":(cmpPerson.compare(p1,p2)<0?" < ":" = "))+ "et p2 " + p2.toString());

        System.out.println("p1: " + p1.toString() +((cmpPerson.compare(p1,p3)>0)?" > ":(cmpPerson.compare(p1,p3)<0?" < ":" = "))+ "et p3 " + p3.toString());

        System.out.println("p3: " + p3.toString() +((cmpPerson.compare(p3,p2)>0)?" > ":(cmpPerson.compare(p3,p2)<0?" < ":" = "))+ "et p2 " + p2.toString());


        System.out.println("\n4- Comparateur de parsonne en, fonction du lastname puis en fonction du firstName (Inverse)");
        System.out.println("p1: " + p1.toString() +((cmpPersonReverse.compare(p1,p2)>0)?" > ":(cmpPersonReverse.compare(p1,p2)<0?" < ":" = "))+ "et p2 " + p2.toString());

        System.out.println("p1: " + p1.toString() +((cmpPersonReverse.compare(p1,p3)>0)?" > ":(cmpPersonReverse.compare(p1,p3)<0?" < ":" = "))+ "et p3 " + p3.toString());

        System.out.println("p3: " + p3.toString() +((cmpPersonReverse.compare(p3,p2)>0)?" > ":(cmpPersonReverse.compare(p3,p2)<0?" < ":" = "))+ "et p2 " + p2.toString());

        Consumer<Person> cons= p-> System.out.println((p==null)?"null":p.toString());
        System.out.println("\n\nAvant triage");

        listePersonne.forEach(cons);
        listePersonne.sort(Comparator.nullsLast(cmpPerson));
        System.out.println("\n\nApres triage");
        listePersonne.forEach(cons);

    }
    private static Comparator<Person> comparing(Function<Person, String> keyExtractor) {
        return (p1, p2) -> (keyExtractor.apply(p1).compareTo(keyExtractor.apply(p2)));
    }



}
