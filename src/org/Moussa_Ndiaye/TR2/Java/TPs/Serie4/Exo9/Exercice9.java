package org.Moussa_Ndiaye.TR2.Java.TPs.Serie4.Exo9;

import java.util.function.Predicate;

public class Exercice9 {
    public static void main(String[] args) {
        String ch1 = "JAVA11";
        String ch2 = "Nemo";

        //- 1:
        Predicate<String> predTaille4 = s -> (s.length() > 4);
        System.out.println("1- Test du predicat taille > 4 sur " + ch1 + " : " + predTaille4.test(ch1));
        System.out.println("   Test du predicat taille > 4 sur " + ch2 + " : " + predTaille4.test(ch2));

        //- 2:
        Predicate<String> predNonVide = s -> !s.isEmpty();
        System.out.println("2- Test du predicat chaine non vide sur " + ch1 + " : " + predNonVide.test(ch1));
        System.out.println("   Test du predicat chaine non vide sur '' " + " : " + predNonVide.test(""));

        //- 3
        Predicate<String> predStartWithJ = s -> s.startsWith("J");
        System.out.println("3- Test du prededicat chaine commence par J sur " + ch1 + " : " + predStartWithJ.test(ch1));
        System.out.println("   Test du prededicat chaine commence par J sur " + ch2 + " : " + predStartWithJ.test(ch2));

        //- 4
        Predicate<String> predTaille5 = s -> (s.length() == 5);
        System.out.println("4- Test du predicat taille chaine == 5sur " + ch1 + " : " + predTaille5.test(ch1));
        System.out.println("   Test du predicat taille chaine == 5sur " + ch2 + " : " + predTaille5.test(ch2));

        //- 5
        Predicate<String> predStartJTaille5 = predStartWithJ.and(predTaille5);
        System.out.println("5- Test du predicat chaine commence par J et de taille 5 " + ch1 + " : " + predStartJTaille5.test(ch1));
        System.out.println("   Test du predicat chaine commence par J et de taille 5 " + ch2 + " : " + predStartJTaille5.test(ch2));

    }
}
