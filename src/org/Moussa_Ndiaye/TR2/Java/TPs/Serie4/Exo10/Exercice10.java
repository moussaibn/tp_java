package org.Moussa_Ndiaye.TR2.Java.TPs.Serie4.Exo10;

import java.util.function.BiFunction;
import java.util.function.Function;

public class Exercice10 {
    public static void main(String[] args) {
        String ch="moussa";
        String mot="a";
        //1
        Function<String,String> upper= String::toUpperCase;
        System.out.println("1- mettre en majuscule de "+ch+" : "+upper.apply(ch) );

        //2
        Function<String,String> ident = s -> (s==null?"":s);
        System.out.println("2- Test de fonction ident() sur "+ch+" : "+ident.apply(ch) );
        System.out.println("   Test de fonction ident() sur "+null+" : "+ident.apply(null) );

        //3
        Function<String,Integer> taille = s ->ident.apply(s).length();
        System.out.println("3- Test de la fonction taille() sur "+ch+" : "+taille.apply(ch) );
        System.out.println("   Test de la fonction taille() sur "+null+" : "+taille.apply(null) );

        //4
        Function<String, String> parenth = s ->"("+ident.apply(s)+")";
        System.out.println("4- Test de la fonction pareth() sur "+ch+" : "+parenth.apply(ch) );
        System.out.println("   Test de la fonction pareth() sur "+null+" : "+parenth.apply(null) );

        //5
        BiFunction<String, String, Integer> position= (s1, s2) ->(s1.indexOf(s2));
        System.out.println("5- Test la fonction position() sur la chaine 'Bonjour'  et le mot 'nj'; position = " +position.apply("Bonjour","nj") );
        System.out.println("   Test la fonction position() sur la chaine 'Bonjour'  et le mot 'Hello'; position = " +position.apply("Bonjour","Hello") );

        //6
        Function<String, Integer> positionBis= s -> (position.apply("abcdefghi",s));
        System.out.println("6- Test de la fonctionpositionBis  sur le mot 'ab'; position = "+positionBis.apply("ab") );
        System.out.println("   Test de la fonctionpositionBis  sur le mot 'xy'; position = "+positionBis.apply("xy") );

    }
}
