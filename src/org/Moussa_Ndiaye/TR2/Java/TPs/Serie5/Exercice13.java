package org.Moussa_Ndiaye.TR2.Java.TPs.Serie5;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Exercice13 {
    public static void main(String[] args) {


        List<String> words = Arrays.asList("one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve");

        //--------------Question 1 -------------------------------------------------------------
        Consumer<String> cons = System.out::println;
        System.out.println("1- Affichage de la liste");
        words.forEach(cons);

        //--------------Question 2 -------------------------------------------------------------

        List<String> quest1 = words.stream()
                .filter(s -> s.length() == 3)
                .collect(Collectors.toList());
        System.out.println("2- Les mots de 3 caractères ");

        quest1.forEach(cons);


        //--------------Question 3 -------------------------------------------------------------
        Comparator<String> cmpLength = (ch1, ch2) -> (Integer.compare(ch1.length(), ch2.length()));

        words = words.stream()
                .sorted(cmpLength)
                .collect(Collectors.toList());
        System.out.println("3- Triage de la liste ");
        words.forEach(cons);


        //--------------Question 4 -------------------------------------------------------------
        Comparator<String> cmpAbc = (ch1, ch2) -> (ch1.compareTo(ch2));

        Comparator<String> cmpLenAbc = cmpLength.thenComparing(cmpAbc);

        words = words.stream()
                .sorted(cmpLenAbc)
                .collect(Collectors.toList());
        System.out.println("4- Triage de la liste ");
        words.forEach(cons);

        //--------------Question 5 -------------------------------------------------------------
        double moy = words.stream()
                .mapToInt(String::length)
                .average().getAsDouble();

        System.out.println("5- la longueur moyenne est " + moy);


        //----------Question 6------------//


        List<String> letters = words.stream()
                .flatMap(
                        s -> s.chars()
                                .mapToObj(Character::toString)
                )
                .collect(Collectors.toList());
        System.out.println("6- Creation du stream letters : ");
        letters.forEach(cons);
        //6-A Le nombre de lettres

        long nbLetters = letters.stream().count();
        System.out.println("Le nombre d'elements est " +nbLetters);

        //6;b Le nombre de lettre different sur le stream

        long nbLettersDiff = letters.stream().distinct().count();
        System.out.println("Le nombre d'elements est distinct " + nbLettersDiff);


        //6-C
        String plusPetit = letters.stream().min(cmpAbc).get();
        String plusGrand = letters.stream().max(cmpAbc).get();

        Consumer<List<String>> petitGrand = s ->
                System.out.println("Plus petit " + plusPetit
                        + "\nPlus grand " + plusGrand);

        petitGrand.accept(letters);


    }

}
