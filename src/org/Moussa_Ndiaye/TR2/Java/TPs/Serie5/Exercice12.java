package org.Moussa_Ndiaye.TR2.Java.TPs.Serie5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;


public class Exercice12 {
    public static void main(String[] args) {

        List<String> maListe=  List.of("one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve");
        Map<Integer, List<String>> map = new HashMap<>();

        //Question 1
        for (String s:maListe){
            map.computeIfAbsent(s.length(),key->new ArrayList<>()).add(s);
        }
        System.out.println("1- Construction de notre table de hachage");
        map.forEach((k,v)-> System.out.println(k + " -> " + v));


        //Question 2
        Map<String, List<String>> map2 = new HashMap<>();

        for (String s:maListe){
            map2.computeIfAbsent(s.substring(0,1),key->new ArrayList<>()).add(s);
        }
        System.out.println("2- Construction de notre 2e table de hachage");

        map2.forEach((k,v)-> System.out.println(k + " -> " + v));


        //Question 3
        Map<String, HashMap<Integer,List<String>>> map3 = new HashMap<>();

        for (String s:maListe){
            map3.computeIfAbsent(s.substring(0,1),key->new HashMap<>()).computeIfAbsent(s.length(),key->new ArrayList<>()).add(s);
        }
        System.out.println("3- Construction de notre 3e table de hachage");

        map3.forEach((k,v)-> System.out.println(k + " -> " + v));

        //Question 4
        Map<Integer, String> map4 = new HashMap<>();
        for (String s:maListe) {
            map4.merge(s.length(), s,(oldValue,newValues)->oldValue+","+newValues);
        }
        System.out.println("4- Construction de notre 4e table de hachage");

        map4.forEach((k,v)-> System.out.println(k + " -> " + v));

    }
}