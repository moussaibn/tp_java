package org.Moussa_Ndiaye.TR2.Java.TPs.Serie6;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;

public class Exercice14 {
    public static List<String> readLinesFrom(String fileName) {
        Path path = Paths.get(fileName);
        try (Stream<String> lines = Files.lines(path)) {
            return lines.collect(Collectors.toList());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public static void main(String[] args) {

        //--------------Question 1------------------------------------
        //Cette fonction noyus permettra d'extraire une partie du fichier
        Function<String, List<String>> lignesFromGerminal = (s) -> {
            long nbLine = readLinesFrom(s).stream().count();
            return readLinesFrom(s).stream()
                    .skip(70)
                    .limit(nbLine - 70 - 322)
                    .collect(Collectors.toList());
        };

        List<String> germinal = lignesFromGerminal.apply("Germinal.txt");

        //--------------Question 2------------------------------------
        //Cette fonction va nous permettre de determiner la taille du document
        Function<Stream<String>, Long> nbligneNonNul = s -> s.filter(l -> !l.isBlank()).count();

        System.out.println("2- Le nombre de ligne du fichier est : " + nbligneNonNul.apply(germinal.stream()));


        //--------------Question 3------------------------------------

        //Bifunction qui cherche le nombre d'occurence d'un mot sur une chaine
        BiFunction<String, String, Long> nbOccurences =
                (ligne, mot) -> {
                    int i = 0;
                    long count = 0;
                    while ((i = ligne.toUpperCase().indexOf(mot.toUpperCase(), i)) != -1) {
                        count++;
                        i++;
                    }
                    return count;
                };

        long nbBonjour = germinal.stream()
                .mapToLong(l -> nbOccurences.apply(l, "Bonjour"))
                .sum();

        System.out.println("3- La chaine 'Bonjour' apparait " + nbBonjour + " dans Germinal");


        //--------------Question 4------------------------------------

        Function<String, Stream<Character>> splitToCharStream = s -> s.chars().mapToObj(c -> (char) c);

        System.out.println("4- Test splitToCharStream avec la chaine 'moussa' = " + splitToCharStream.apply("moussa").collect(Collectors.toList()));

        //--------------Question 5------------------------------------

        //Cette fonction va permettre de separer une
        Function<Stream<String>, Stream<Character>> textToCharStream = s -> s.flatMap(splitToCharStream);

        Stream<Character> germinalChar = textToCharStream.apply(germinal.stream());

        //--------------Question 6------------------------------------

        Stream<Character> germinalCharSansDoublons = germinalChar.distinct().sorted(Comparator.naturalOrder());

        //Les caracteres qui ne sont pas des lettres

        Stream<Character> notLetters = germinalCharSansDoublons
                .filter(
                        l -> !l.toString().matches("[a-zA-Z]"));

        String chNotLetters = notLetters.map(c -> c.toString()).collect(Collectors.joining())
                .replace("-", "\\-");

        System.out.println("6- Les caracteres qui ne sont pasn des lettres : " + chNotLetters);

        BiFunction<String, String, Stream<String>> splitWordWithPattern =
                (line, pattern) -> Pattern.compile("[" + pattern + "]").splitAsStream(line);

        splitWordWithPattern
                .apply("  Bonjour le-monde!", chNotLetters).forEach(System.out::println);


        //--------------Question 7------------------------------------

        System.out.println("7- Nombre de mots au total " + germinal.stream().flatMap(l -> splitWordWithPattern.apply(l, chNotLetters)).count());
        System.out.println("   Nombre de mots distinct " + germinal.stream().flatMap(l -> splitWordWithPattern.apply(l, chNotLetters)).distinct().count());


        //--------------Question 8------------------------------------

        Map<Integer, List<String>> tableMots = germinal.stream().
                flatMap(l -> splitWordWithPattern.apply(l, chNotLetters))
                .collect(groupingBy
                        (
                                String::length
                        )
                );
        Map.Entry<Integer, List<String>> maxi = tableMots.entrySet().stream().max(Comparator.comparing(Map.Entry::getKey)).get();
        System.out.println("8- la longueur du mot le plus long utilisé : " + maxi.getKey());

        //--------------Question 9-----------------------------------


        System.out.println("9- Germinal comporte " + maxi.getValue().size() + " de cette longueur");

        System.out.println("   Les mots les plus longs ont " + maxi.getValue().stream().distinct().collect(Collectors.toList()));

        //--------------Question 10------------------------------------

        Map<String, Long> freqMots = germinal.stream()
                .flatMap(l -> splitWordWithPattern.apply(l, chNotLetters))
                .filter(e -> !e.isBlank() && !e.contains("e"))
                .collect(groupingBy(e -> e, Collectors.counting())
                );
        Map<Long, List<String>> memeFreq = freqMots.entrySet()
                .stream()
                .collect(groupingBy(
                        Map.Entry::getValue,
                        Collectors.mapping(Map.Entry::getKey, Collectors.toList()))
                );


        Map.Entry<Long, List<String>> mostUsed = memeFreq.entrySet().stream()
                .max(Comparator.comparing(Map.Entry::getKey)).get();

        System.out.println("10- Le mot le plus utilisé ne contenant pas de 'e' est : " + mostUsed.getValue() +
                " on l'a utilisé " + mostUsed.getKey() + " fois");
    }


}

