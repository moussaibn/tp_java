# Projet Java 

* ** Moussa Ndiaye** - *Eleve ingenieur en Telecommunications et reseaux*

## Serie 0

Dans cette premiere serie, il s'agissait de prendre en main le logiciel et de creer nos premieres classes Java

### Exercice 1: Bonjour
Le but de ce deuxiéme exercice est de prendre en main les fonctions de base d'Eclipse.

```
Bonjour tout le monde
```

## Serie 1

### Exercice 2: Factorielle
On veut creer une classe permettant de calculer la factorielle d'un nombre.

```
Factorielle d'un entier: 10! = 3628800
Factorielle d'un double: 13! = 6.2270208E9
Factorielle d'un BigInteger 25! = 15511210043330985984000000

```


### Exercice 3: Bissextile 
On souhaite écrire une méthode qui permette de détecter qu'une année est bissextile.
Une année est bissextile si elle est divisible par **4** mais non divisible par **100**, à moins qu'elle soit
divisible par **400**.

```
l'annee 1991 bissextile : false
l'annee 1996 bissextile : true
l'annee 1900 bissextile : false
l'annee 2000 bissextile : true

```

### Exercice 4: Palindrome
On souhaite écrire une méhode qui permette de détecter qu'une phrase est un palindrome. 
Un palindrome est une phrase que l'on peut lire dans les deux sens, 
sans tenir compte de la ponctuation ou des espaces.

```
MATAMr est un palindrome : false
Rions noir est un palindrome : true
Esope reste ici et se repose est un palindrome : true
Elu par cette crapule est un palindrome : true
Et la marine va venir a Malte est un palindrome : true
```

## Serie 2

### Exercice 5:

Le but de cet exercice est de créer une classe **Marin**, qui comportera les champs suivants :
* nom, 
* prenom,
* salaire. 
Les champs de cette classe seront déclarés de telle sorte qu'ils seront inaccessibles de l'extérieur.

```
Marin m1:
Prenom :Ndiaye
Nom :Moussa
Salaire :2500

Marin m2:
Prenom :Ndiaye
Nom :Moussa
Salaire :2500

Marin m3:
Prenom :Matin
Nom :Martin
Salaire :500

Les marins m1 et m2 sont egaux true
Les marins m1 et m3 sont egaux false

Affichage des hashcode
Pour m1:132606444
Pour m2:132606444
Pour m3:1374304456

```


### Exercice 6:

On reprend la classe `Marin` de l'exercice 5. On a crée une classe **MarinUtil** qui contiendra les méthodes :
* augmenteSalaire(Marin[] marins, int pourcentage) 
* getMaxSalaire(Marin[] marins) 
* getMoyenneSalaire(Marin[] marins)
* getMedianeSalaire(Marin[] marins)


```
Prenom :Ndiaye
Nom :Moussa
Salaire :2800

Prenom :Camara
Nom :Alioune
Salaire :3000

Prenom :Rannou
Nom :Nicolas
Salaire :1500

Apres une augmentation de 15%
Prenom :Ndiaye
Nom :Moussa
Salaire :3220

Prenom :Camara
Nom :Alioune
Salaire :3450

Prenom :Rannou
Nom :Nicolas
Salaire :1725

Le salaire maximum est 3450
Le salaire moyen est 1725.0
Le salaire median est 3450.0
```


## Serie 3

### Exercice 7:

Dans cette exercice on crée une  classe ** Equipage ** qui contiendra une collection de ** Marins**.
On reprendra la classe ** Marins** de **l'exercice 5 **.
Cette classe ** Equipage ** contiendra les méthodes suivantes:  
* addMarin()
* removeMarin()
* isMarinPresent()

```
TEST ADD:
Le nombre de marins est 4

Marin 0
Prenom :Ndiaye
Nom :Moussa
Salaire :2800

Marin 1
Prenom :Camara
Nom :Alioune
Salaire :3000

Marin 2
Prenom :G
Nom :Dinesh
Salaire :2400

Marin 3
Prenom :Rannou
Nom :Nicolas
Salaire :1500

Test du addALL:
Le nombre de marins est 5

Marin 0
Prenom :Ndiaye
Nom :Moussa
Salaire :2800

Marin 1
Prenom :Camara
Nom :Alioune
Salaire :3000

Marin 2
Prenom :G
Nom :Dinesh
Salaire :2400

Marin 3
Prenom :Rannou
Nom :Nicolas
Salaire :1500

Marin 4
Prenom :thinkpad
Nom :lenovo
Salaire :800

TEst clear:
Le nombre de marins est 0
```

Modification pour creer une classe Equipage avec un nombre limite de marins

```
TEST ADD: 
Le nombre de marins est 4

Marin 0
Prenom :Ndiaye
Nom :Moussa
Salaire :2800

Marin 1
Prenom :Camara
Nom :Alioune
Salaire :3000

Marin 2
Prenom :G
Nom :Dinesh
Salaire :2400

Marin 3
Prenom :Rannou
Nom :Nicolas
Salaire :1500

Ajout impossible, limite atteinte
Test du addALL:
Le nombre de marins est 4

Marin 0
Prenom :Ndiaye
Nom :Moussa
Salaire :2800

Marin 1
Prenom :Camara
Nom :Alioune
Salaire :3000

Marin 2
Prenom :G
Nom :Dinesh
Salaire :2400

Marin 3
Prenom :Rannou
Nom :Nicolas
Salaire :1500

TEst clear: 
Le nombre de marins est 0

```


### Exercice 8:

```
Affichage du contenu du registre:0 marins
Test toString:
RegistreMarin [map={}]
TEST addMarin
7 marins
RegistreMarin [map={1970=[Prenom :Armel
Nom :Le Cl�ac'h
Annee naissance :1977
, Prenom :Riou
Nom :Vincent
Annee naissance :1972
], 1960=[Prenom :Alain
Nom :Gautier
Annee naissance :1962
, Prenom :Desjoyeaux
Nom :Michel
Annee naissance :1965
], 1980=[Prenom :Gabart
Nom :François
Annee naissance :1983
], 1950=[Prenom :Titouan
Nom :Lamazou
Annee naissance :1955
, Prenom :Christophe
Nom :Auguin
Annee naissance :1959
]}]
Les Marins 2 nés dans les annees 60
[Prenom :Alain
Nom :Gautier
Annee naissance :1962
, Prenom :Desjoyeaux
Nom :Michel
Annee naissance :1965
]

```

## Serie 4


### Exercice 9:
>Question 1:

Le prédicat `predTaille4` prend une chaîne de caractères en entrée et retourne true si cette chaîne fait plus de 4 caractères.
   
``` 
1- Test du predicat taille > 4 sur JAVA11 : true
   Test du predicat taille > 4 sur Nemo : false
```
>Question 2:

Le prédicat `predNonVide` prend une chaîne de caractères en entrée et retourne true si cette chaîne est non vide.
``` 
2- Test du predicat chaine non vide sur JAVA11 : true
   Test du predicat chaine non vide sur ''  : false
```
>Question 3:

Le prédicat `predStartWithJ` prend une chaîne de caractères en entrée et retourne true si cette chaîne commence par un J.
``` 
3- Test du prededicat chaine commence par J sur JAVA11 : true
   Test du prededicat chaine commence par J sur Nemo : false
```
>Question 4:

Le prédicat `predTaille5` prend une chaîne de caractères en entrée et retourne true si cette chaîne
fait exactement 5 caractères de long.
``` 
4- Test du predicat taille chaine == 5sur JAVA11 : false
   Test du predicat taille chaine == 5sur Nemo : false
```
>Question 5:

Le prédicat `predStartJTaille5` prend une chaîne de caractères en entrée et retourne true si cette chaîne commence par un J et fait exactement 5 caractères de long.
``` 
5- Test du predicat chaine commence par J et de taille 5 JAVA11 : false
   Test du predicat chaine commence par J et de taille 5 Nemo : false
```

### Exercice 10:
>Question 1:

La fonction`upper()` prend une chaîne de caractères en paramètre et retourne cette chaîne en majuscules.
``` 
1- mettre en majuscule de moussa : MOUSSA
```
>Question 2:

La fonction `ident()` prend une chaîne de caractères en paramètre et retourne cette même chaîne, sans modification. Si la chaîne passée est nulle, alors elle doit retourner la chaîne vide.
``` 
2- Test de fonction ident() sur moussa : moussa
   Test de fonction ident() sur null : 
```

>Question 3:

La fonction `taille()` prend une chaîne de caractères en paramètre et retourne la longueur de
cette chaîne. Si la chaîne passée en paramètre est nulle, alors elle doit retourner 0.

``` 
3- Test de la fonction taille() sur moussa : 6
   Test de la fonction taille() sur null : 0
```

>Question 4:

La fonction  prend `parenth` une chaîne de caractères en paramètre et retourne cette chaîne
entre parenthèses. Si la chaîne passée est nulle, alors le résultat est une parenthèse ouvrante
immédiatement suivie d’une parenthèse fermante.
``` 
4- Test de la fonction pareth() sur moussa : (moussa)
   Test de la fonction pareth() sur null : ()
```
>Question 5:

La bifonction `position()` prend deux chaînes de caractères en paramètres. Cette fonction doit retourner un entier qui est la première position de la deuxième chaîne dans la première. Si la deuxième chaîne ne se trouve pas dans la première, alors cette fonction doit retourner -1.
``` 
5- Test la fonction position() sur la chaine 'Bonjour'  et le mot 'nj'; position = 2
   Test la fonction position() sur la chaine 'Bonjour'  et le mot 'Hello'; position = -1
```
>Question 6:

La fonction qui prend une chaîne de caractères en paramètre. Cette fonction doit retourner un entier qui est la position de cette chaîne dans la chaîne « abcdefghi ». Si cette chaîne ne s’y trouve pas, elle doit retourner -1. 

``` 
6- Test de la fonctionpositionBis  sur le mot 'ab'; position = 0
   Test de la fonctionpositionBis  sur le mot 'xy'; position = -1
```


### Exercice 11:
>Question 1:

Le comparateur `cmpLength` permet de comparer deux caines en fonction de leur longueur
``` 
1- Comparateur de longueur de chaines de caractere
Moussa = Aliou
``` 
>Question 2:

le comparateur `cmpLastName` permet de comparer deux **lastname** de personnes en utilisant l'ordre alphabetiquee
``` 
2- Comparateur de parsonne en, fonction du lastname
p1: Moussa Ndiaye  = et p2 Meissa Ndiaye 
```
>Question 3:

Le comparateur de personne `cmpPerson` compare les personnes en fonction de leurs noms de
   famille, et si ces personnes ont même noms de famille, en fonction de leurs prénoms.

``` 
3- Comparateur de parsonne en, fonction du lastname puis en fonction du firstName
p1: Moussa Ndiaye  > et p2 Meissa Ndiaye 
p1: Moussa Ndiaye  > et p3 Aliou Camara 
p3: Aliou Camara  < et p2 Meissa Ndiaye 
```
>Question 4:

Le comparateur `cmpPersonReverse` fait le contraire de comparateur precedent:
``` 
4- Comparateur de parsonne en, fonction du lastname puis en fonction du firstName (Inverse)
p1: Moussa Ndiaye  < et p2 Meissa Ndiaye 
p1: Moussa Ndiaye  < et p3 Aliou Camara 
p3: Aliou Camara  > et p2 Meissa Ndiaye 
```

Comparateur pour le tri d'une liste avec des valeurs nulles:
``` 

Avant triage
null
Moussa Ndiaye 
Meissa Ndiaye 
null
Aliou Camara 
Zinedine Zidane 
null
null


Apres triage
Aliou Camara 
Meissa Ndiaye 
Moussa Ndiaye 
Zinedine Zidane 
null
null
null
null
```
## Serie 5

### Exercice 12:
>Question 1:

Construction d'une table de hachage dont les clés sont les longueurs des chaînes de caractères et les
valeurs les listes des chaînes qui ont cette longueur.
```
1- Construction de notre table de hachage
3 -> [one, two, six, ten]
4 -> [four, five, nine]
5 -> [three, seven, eight]
6 -> [eleven, twelve]
```
>Question 2:

Construction d'une table de hachage dont les clés sont la première lettre de chaque chaîne (sous forme d’une chaîne de caractères) et les valeurs les listes des chaînes qui ont cette première lettre.
```
2- Construction de notre 2e table de hachage
s -> [six, seven]
t -> [two, three, ten, twelve]
e -> [eight, eleven]
f -> [four, five]
n -> [nine]
o -> [one]
```
>Question 3:

Construction d'une table de hachage dont lla première lettre de chaque chaîne (sous forme d’une chaîne de caractères) et les valeurs des tables de hachage dont les clés sont les longueurs de chaînes et les valeurs les listes de ces chaînes.

```
3- Construction de notre 3e table de hachage
s -> {3=[six], 5=[seven]}
t -> {3=[two, ten], 5=[three], 6=[twelve]}
e -> {5=[eight], 6=[eleven]}
f -> {4=[four, five]}
n -> {4=[nine]}
o -> {3=[one]} 
```
>Question 1:

Construction d'une table de hachage dont les clés sont la longueur des chaînes et les valeurs des chaînes de caractères, résultat de la concaténation de ces chaînes, séparés par des virgules.
``` 
4- Construction de notre 4e table de hachage
3 -> one,two,six,ten
4 -> four,five,nine
5 -> three,seven,eight
6 -> eleven,twelve

```

### Exercice 13:
>Question 1:

On cree un **Consumer** que l'on appelle sur chaque element de la liste via un `forEach`.
Le resultat donne
```
1- Affichage de la liste
one
two
three
four
five
six
seven
eight
nine
ten
eleven
twelve

```
>Question 2:

On applique un  filtre un filtre simple sur le stream de la liste de mot

```
2- Les mots de 3 caractères 
one
two
six
ten
```
>Question 3:

On trie la liste en appellant la methode`sorted()`de l'**API Stream**. Ici on trie selon la longueur des mots
```
3- Triage de la liste 
one
two
six
ten
four
five
nine
three
seven
eight
eleven
twelve
```
>Question 4:

Dans cette partie, on n trie la liste en appellant la methode`sorted()`de l'**API Stream**. Ici on trie selon la longueur des mots puis selon l'ordre alphabetique
```
4- Triage de la liste 
one
six
ten
two
five
four
nine
eight
seven
three
eleven
twelve
```
>Question 5:

On recuppere la longueur moyenne en utilisant la methode `.average()`

``` 
5- la longueur moyenne est 4.25
```
>Question 6:
* Le nombre de lettre de ce **Stream**
On utilise la methode `.count()` pour connaitre le nombre d'element
```
Le nombre d'elements est 51
```
* Le nombre d'elements distincts

On utilise d'abord un `.distinct`par la suite, on utilise la methode `.count()` pour connaitre le nombre d'element
```
Le nombre d'elements est distinct 15
```

* Le nombre d'elements distincts

On utilise la methode `.min()` et `.max()` auxquels on passer un comparateur
```
Plus petit e
Plus grand x
```

## Serie 6
### Exercice 14:
Dans cette exercice, il s'agissait d'approfondir la prise en main de l'API Stream et les differentes fonctions pour manipuler de grosses quantites de donnees,
>Question 1:

On a crée une methode `lignesFromGerminal()` qui prend en  entree le nom d'un fichier et place les lignes( de la **70** à la **fin-322** )dans une liste.
  
>Question 2:

Pour cette question, on a crée une fonction `nbligneNonNul()` qui prend un stream en parametre et renvoie le nombre de ligne non vides ou non formés de **blancs**
En l'appliquant a notre text on a :
```
2- Le nombre de ligne du fichier est : 16298
```

>Question 3

Ici on va creer une **biFonction** `nbOccurence()` qui prend en parametre une ligne du texte et un mot et qui renvoie le le nombre d'occurence de ce **mot** sur la **ligne**.
Ensuite on applique cette fonction à tout le texte. On a à l'excecution:
```
3- La chaine 'Bonjour' apparait 7 dans Germinal 
```

>Question4 :

Pour realiser cette question, nous creer la fonction `splitToCharStream()` qui prend en parametre une chaine de careatere qui va constituer notre ligne et renvoie un stream de chaine de caractere.
On va tester cette fonction en l'appliquant à une chaine de caractere 
```
4- Test splitToChar avec la chaine 'moussa' = [m, o, u, s, s, a]
```

>Question 5

Ici on va reutiliser la fonction precedente dans une fonction `textToCharStream()` qu'on applique à toutes les lignes du texte pour convertir le texte en un stream de **Character**


>Question 6

On va placer dans la variable de type **Stream** nommée `germinalCharSansDoublons` tous les caracteeres se trouvant sur le texte.Par la suite on applique un filtre sur ce **Stream** pour reccuperer tous les caracteres qui ne sont pas des lettres d'abord dans un **Stream** `notLetters` puis dans une chaine  `chNotLetters`. On ecchape ensuite le caractere`-` en plaçant `\\`devant.
 
L'affichage donne:
```
6- Les caracteres qui ne sont pasn des lettres :  !'*,\-.012456789:;<>?_
```
On peut à prpesent utiliser la **biFunction** `splitWordWithPattern()` pour decouper notre texte en fonction denotre liste de separateur!

En appliquant la fonction à la chaine `  Bonjour le-monde!`, on a:

```
Bonjour
le
monde
``` 
>Question 7

On a effectué les calcul en usant de **l'API Stream**
* Pour le nombre de mots total, on decoupe le texte en une liste de **Stream** de mots ensuite on fait le decompte.

* Pour le nombre de mots differents, on decoupe le texte en une liste de **Stream** de mots ensuite on fait le decompte des elements differents.

```
7- Nombre de mots au total 218709
  Nombre de mots distinct 13933
```

>Question 8

Pour realiser cette question, On va faire un groupby pour recupperer en sortie de ce **Stream** une liste de paire *(cle, valeur)* contitué de *(taille_mot,liste_mots)*
Pour avoir le mot le plus long, on utilise la methode   `.max()`
```
8- la longueur du mot le plus long utilisé : 19
```

>Question 9

On utilise la paire calculée avec la question precedente
```
9- Germinal comporte 3 de cette longueur
   Les mots les plus longs sont [proportionnellement, revolutionnairement]
```
   
>Question 10

Pour repondre à cette question, on determine la frequence de tous les mots du texte ne contenant pas la lettre 'e' ensuite on cherche le max.
```
10- Le mot le plus utilisé ne contenant pas de 'e' est : [la] on l'a utilisé 5785 fois
```

## Serie 7
### Exercice 15
> Question 1

Les `streams` etendent de `BaseStream` qui comporte une methode **close()** et qui implémente **AutoCloseable**.

En règle générale, seuls les `Stream` dont le source est un canal IO doivent être fermés et cette fermeture pourrait être gerée lorsqu'il est declaré comme  ressource dans une instruction `try-with-resources`.

> Question 3.c:

Il serait possible de faire appel à un **flatMap** pour lires les personnnes à partir d'un fichier. En effet notre fonction `mapper` pourra renvoyer un `Stream` de personnes lorsqu'il trouve une personne ou un `Stream.Empty()` lorsau'on ne trouve pas une personne. Le flatMap va agir en laissant passer que les `Stream` non  vide.

# Serie 8
### Exercice 16
>Question 1.a

Les classes `BufferedInputStream` et `BufferedOutputStream` creent des buffers en memoire pour la lecture ou l'ecriture d'octets sur un fichier.
>Questtion 1.b

Les classes `DataInputStream et DataOutputStream` permettent de lire ou d'ecrire des types primitif sur un Buffer. On dispose des methodes:

| Type      | Ecriture            | Lecture      |
| :-------: |  :---------------:  | :--------:   |
| int       | writeInt(int)       | readInt()    |
| double    | writeDouble(double) | readDouble() |
| char      | writeChar(char)     | readChar()   |
| float     | writeFloat(float)   | readFloat()  |
| long      | writeLong(long)     | readLong()   |
| String    | writeUtf(String)    | readUtf()    |     

> Question 3.a

Du fait que les donnees aient été persistées en memoire sous une suite de tableaux d'octets, on doit les lire de la meme maniere c'est a dire tableau d’octets par tableau d’octets 
>Question 3.b

Pour relire le bon  nombre de personnes, il est important d'indiquer dans le fichier la taille de notre liste

### Exercice 17

>Question 1:

Pour ecrire les objets Java directement sur un fichier, on doit utiliser les flux: 
* ObjectInputStream : pour lire un objet dans un fichier/buffer
* ObjectOutputStream : pour ecrire un objet dans le fichier/buffer 

>Question 2: 

Pour ecrire des instances de la `Person` directement dans un fichier, on doit s'assurer que la classe est **serializable**

>Question 6:

Dans le fichier, on  note la presence des classes
* la classe `org.Moussa_Ndiaye.TR2.Java.TPs.Serie8.Exercice17.Person` qui est le nom complet de la classe **Person**
* La classe `java/lang/String` qui est le nom complet de la classe **String**.

Ce mecanisme ne pourra marcher qui si  les classes sont **serializable**. Dans notre cas ce programme va s'executer correctement car la classe `String` implemente l'interface serializable.

##Serie 9
### Exerrcice 19:
>Question 2:

La methode `getInstance(String className)` ne pourra instancier la classe passé en parametre seulement si cette derniere contient le constructeur par defaut. En effet pour creer une instance, cette methode ne dispose que d'un  champs qui contient le nom de la classe. Donc on ne pourra pas fournir de parametres à la classe ce qui nous oblige à utiliser le constructeur par defaut.

>Question 4:

Notre methode `get(Object bean, String property)` aura comme type de retour **Object** car on ne connait pas à priori ce qu'elle renvoie et la classe **Object** est la `SuperClasse` de toute les classes en Java